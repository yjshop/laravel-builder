<div align="center">

# LARAVEL-Builder

这是一个可以提升`Laravel ORM`关联关系查询性能的扩展包，


</div>

## 环境

- PHP >= 7
- laravel >= 5.5

## 安装

```bash
composer require eugenes/laravel-builder
```

### 简介

`Laravel`的关联关系查询`whereHas`在日常开发中给我们带来了极大的便利，但是在**主表**数据量比较多的时候会有比较严重的性能问题，主要是因为`whereHas`用了`where exists (select * ...)`
这种方式去查询关联数据。

通过这个扩展包提供的`whereHasIn,whereHasJoin`方法

## 方法

### whereHasIn

```php
App(CompanyStaffModel::class)->whereHasIn('section', function ($query) {
    $query->where('id', 1);
})->first();
```

```sql
SELECT *
FROM `lk_company_staff`
WHERE `lk_company_staff`.`id` IN (SELECT `lk_company_staff_section_relation`.`userid`
                                  FROM `lk_company_staff_section_relation`
                                  WHERE `lk_company_staff`.`id` = `lk_company_staff_section_relation`.`userid`
                                    AND `id` = 1) LIMIT 1
```

### whereHasJoin

```php
App(CompanyStaffModel::class)->yjselect('*')->whereHasJoin('section', function ($query) {
         $query->yjwhere('id', 1);
})->first();
```

```sql
select `lk_company_staff`.*
from `lk_company_staff`
         left join `lk_company_staff_section_relation`
                   on `lk_company_staff_section_relation`.`userid` = `lk_company_staff`.`id`
where ((`lk_company_staff_section_relation`.`id` = 1)) limit 1
```

### yjwhere

```php
App(CompanyStaffModel::class)->yjwhere('id', 1)->first();
```

```sql
select *
from `lk_company_staff`
where `lk_company_staff`.`id` = 1 limit 1
```

### yjselect

```php
App(CompanyStaffModel::class)->yjselect('id')->first();
```

```sql
select `lk_company_staff`.`id`
from `lk_company_staff` limit 1
```

### yjsum

### yjpluck

### yjorderBy

### yjorderByDesc

### orWhereHasIn

### orWhereHasNotIn

### whereHasMorphIn

### orWhereHasMorphIn

## License

[The MIT License (MIT)](LICENSE).
