<?php

namespace Eugenes\LaravelBuilder\Tests\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'test_histories';
}
