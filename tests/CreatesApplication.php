<?php

namespace Eugenes\LaravelBuilder\Tests;

use Eugenes\LaravelBuilder\WhereHasInServiceProvider;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../vendor/laravel/laravel/bootstrap/app.php';

        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

        $app->register(WhereHasInServiceProvider::class);

        return $app;
    }
}
